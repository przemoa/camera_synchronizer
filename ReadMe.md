# 0. System #
  - Linux, tested on Linux Mint
  - http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment
  
# 1. Requirements #
- Środowisko ROS, instalacja:
  ```
  sudo apt-get install ros-kinetic-desktop
  mkdir -p ~/catkin_ws/src
  cd ~/catkin_ws/
  catkin_make
  ```
- Python 2
- przydatne pakiety:
    ```
    sudo apt-get install ros-kinetic-image-view
    sudo apt-get install ros-kinetic-usb-cam 
    sudo apt-get install ros-kinetic-rqt
    ```

# 2. ROS basic usage
 - a) Global ROS environment setup:
    ``` 
    . /opt/ros/kinetic/setup.bash
    ```
    poprzedzone zmianą uprawnień, jeżeli nie są ustawione:
    ```
    sudo chmod +x /opt/ros/kinetic/setup.bash 
    ```
    Aby zweryfikować poprawność:
    ```
    printenv | grep ROS
    ```
 - b) Instalacja tutoriali dostępnych pod adresem [http://wiki.ros.org/ROS/Tutorials]
    ```
    sudo apt-get install ros-kinetic-ros-tutorials 
    ```
 - c) Start ROS core:
    ```
    roscore
    ```
 - d) Wyświetlenie listy nodów:
    ```
    rosnode list
    ```
 - e) Wyświetlenie listy topiców:
    ```
    rostopic list
    ```
 - f) Wizualne wyświetlenie połączeń:
    ```
    rosrun rqt_graph rqt_graph
    ```
 - g) Test kamery:
    ```
    rosrun usb_cam usb_cam_node
    rosrun image_view image_view image:=/usb_cam/image_raw
    ```
    
# 3. Instalacja pakietu camera_synchronizer
 - Wykonać polecenia:
    ```
    cd ~/catkin_ws/src
    git clone https://gitlab.com/przemoa/camera_synchronizer.git
    cd ..
    . /opt/ros/kinetic/setup.bash
    . devel/setup.bash 
    catkin_make
    ```
    
# 4. Uruchomienie pakietu
 - a) Dla każdej powłoki:
    ```
    cd ~/catkin_ws
    . /opt/ros/kinetic/setup.bash
    . devel/setup.bash
    ```
 - b) w kolejnych powłokach:
    ```
    roscore
    ```
    ```
    rosrun camera_synchronizer 
    ```
    ```
    rosrun camera_synchronizer camera_synchronizer.py
    ```
    
# 5. Informcje dodatkowe:
 - Tworzenie pakietu:
    ```
    catkin_create_pkg camera_synchronizer std_msgs rospy roscpp
    ```
 - Konieczne dodatkowo (przy wykorzystaniu rospy):
   - modyfikacja pliku _package.xml_ w celu dodania zależności od innych pakietów
   - utworzenie pakietu Pythona w folderze _src_ pakietu
   - modyfikacja pliku _CMakeLists.txt_ w celu podania plików pakietu
 - Podglad kamery:
    ```
    gst-launch-0.10 v4l2src device=/dev/video0 ! xvimagesink
    ```
    
# 6. Kalibr instalacja:
 - instalacja zaleznosci
    ```
    easy_install python-igraph
    sudo apt-get install libv4l-dev
    ```
 - instalacja kalibr
    ```
    cd ~/catkin_ws/src
    git clone https://github.com/ethz-asl/Kalibr.git
    cd ..
    catkin_make -DCMAKE_BUILD_TYPE=Release -j4
    ```
    
   
# 7. Kalibr usage
 - podglad na osi czasu rosbaga. Mozliwy rowniez podglad obrazkow (PPM na osi czasu, show image) 
    ```
    rqt_bag ~/.ros/nagranieG.bag
    ```
 - nagrywanie obrazu
    ```
    rosrun usb_cam usb_cam_node _video_device:=/dev/video1
    rosrun usb_cam usb_cam_node __name:=usb_cam1 _video_device:=/dev/video1
    rosrun image_view image_view image:=/supertopik_A
    rosbag record --output-name=plikA --duration=10 supertopik_A supertopik_B 
    ```
 - Wykorzystanie roslaunch do nagrywania
    ```
    cd ~/catkin_ws
    . /opt/ros/kinetic/setup.bash
    . devel/setup.bash
    cd src/camera_synchronizer
    roslaunch ./src/camera_synchronizer/cam_to_bag.launch
   ```
 - default bag folder: ~/.ros/*
 - kalibr kalibracja z nagrania:
    ```
    cd ~/catkin_ws
    . /opt/ros/kinetic/setup.bash
    . devel/setup.bash
    cd src/camera_synchronizer
    kalibr_calibrate_cameras --bag ~/.ros/nagranieB.bag --topics supertopik_A supertopik_B --models pinhole-radtan omni-radtan --target target.yaml --verbose
    ```

# 8. ROSBAG to jpg
    ```
    sudo apt-get install mjpegtools
    sudo apt-get install ffmpeg
    sudo apt-get install mencoder
    rosrun image_view extract_images _sec_per_frame:=0.01 image:=<IMAGETOPICINBAGFILE>&
    rosbag play <BAGFILE>
    ```

    
