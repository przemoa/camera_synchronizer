Calibration results 
====================
Camera-system parameters:
	cam0 (supertopik_A):
	 type: <class 'aslam_cv.libaslam_cv_python.DistortedPinholeCameraGeometry'>
	 distortion: [ 0.1030799  -0.16721339 -0.0012643  -0.00070464] +- [ 0.00789066  0.02875779  0.00096525  0.00129974]
	 projection: [ 1381.97893957  1380.53802029   931.76305636   524.2140363 ] +- [ 5.52062733  5.58509657  4.65409297  3.63990947]
	 reprojection error: [-0.000001, -0.000001] +- [0.249451, 0.231839]



Target configuration
====================

  Type: checkerboard
  Rows
    Count: 5
    Distance: 0.1 [m]
  Cols
    Count: 8
    Distance: 0.1 [m]
