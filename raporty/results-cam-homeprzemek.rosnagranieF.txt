Calibration results 
====================
Camera-system parameters:
	cam0 (supertopik_A):
	 type: <class 'aslam_cv.libaslam_cv_python.DistortedOmniCameraGeometry'>
	 distortion: [ nan  nan  nan  nan] +- [ nan  nan  nan  nan]
	 projection: [ nan  nan  nan  nan  nan] +- [ nan  nan  nan  nan  nan]
	 reprojection error: [nan, nan] +- [nan, nan]



Target configuration
====================

  Type: checkerboard
  Rows
    Count: 5
    Distance: 0.1 [m]
  Cols
    Count: 8
    Distance: 0.1 [m]
