#! /usr/bin/env python

#  Ros imports
import rospy
from random import randint

import message_filters
from std_msgs.msg import Int32, Float32
from sensor_msgs.msg import Image as Imagex


pub1 = rospy.Publisher('supertopik_A', Imagex, queue_size=10)
pub2 = rospy.Publisher('supertopik_B', Imagex, queue_size=10)


last_time = 0;
def callback(img1, img2):
    #pass
    global last_time
    # TODO TODO TODO recznie wybierac co ktoras klatke
    # print(str(img1.header.stamp) + '\n')

    current_time = int(str(img1.header.stamp)) / 1000000;
    if current_time - last_time > 250:
        last_time = current_time
        pub1.publish(img1)
        pub2.publish(img2)


    # print (int(str(img1.header.stamp)) / 1000000000)
   #The callback processing the pairs of numbers that arrived at approximately the same time


if __name__ == '__main__':
    print('my friend!')

    rospy.init_node('camera_synchronizer')
    camera1_sub = message_filters.Subscriber('/usb_cam1/image_raw', Imagex)
    camera2_sub = message_filters.Subscriber('/usb_cam2/image_raw', Imagex)

    ts = message_filters.ApproximateTimeSynchronizer([camera1_sub, camera2_sub], 10, 0.1, allow_headerless=True)
    #print(dir(ts))
    ts.registerCallback(callback)
    rospy.spin()